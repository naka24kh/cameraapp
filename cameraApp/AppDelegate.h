//
//  AppDelegate.h
//  cameraApp
//
//  Created by naka on 2019/10/02.
//  Copyright © 2019 naka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

