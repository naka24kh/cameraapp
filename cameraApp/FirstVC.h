//
//  ViewController.h
//  cameraApp
//
//  Created by naka on 2019/10/02.
//  Copyright © 2019 naka. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AdSupport/ASIdentifierManager.h>

@interface FirstVC: UIViewController <UITextFieldDelegate,NSURLSessionDelegate>


@property (weak, nonatomic) IBOutlet UITextField *userTF;
@property (weak, nonatomic) IBOutlet UITextField *passwardTF;

@property (nonatomic) NSArray* firstArray;

@end

