//
//  BaseVC.h
//  cameraApp
//
//  Created by naka on 2019/10/17.
//  Copyright © 2019 naka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController

@property (nonatomic) NSArray *jsonDatas;

@end
