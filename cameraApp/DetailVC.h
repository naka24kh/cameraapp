//
//  ViewController+DetailVC.h
//  cameraApp
//
//  Created by naka on 2019/10/04.
//  Copyright © 2019 naka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>


@interface DetailVC : UIViewController <WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UILabel *cameraTag;

@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet WKWebView *detailView;

@property (nonatomic) NSString *detailUrl;
@property (nonatomic) NSString *tagName;

@end


