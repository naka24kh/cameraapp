//
//  containerVC.m
//  cameraApp
//
//  Created by naka on 2019/10/17.
//  Copyright © 2019 naka. All rights reserved.
//


#import "BaseVC.h"
#import "SecondVC.h"

#import <UIKit/UIKit.h>

@interface BaseVC ()

@end

@implementation BaseVC


- (UITraitCollection *)overrideTraitCollectionForChildViewController:(UIViewController *)childViewController{
    UITraitCollection *any = [UITraitCollection traitCollectionWithUserInterfaceStyle:0];
    UITraitCollection *compact = [UITraitCollection traitCollectionWithVerticalSizeClass:1];
    UITraitCollection *regular = [UITraitCollection traitCollectionWithHorizontalSizeClass:2];
    
    //縦の長さが横の長さより大きい時
    if ([[UIScreen mainScreen]bounds].size.height > [[UIScreen mainScreen]bounds].size.width ){
        NSArray *traitArray = [NSArray arrayWithObjects: regular, compact, nil];
        UITraitCollection *combinedTraits = [UITraitCollection traitCollectionWithTraitsFromCollections: traitArray];
        return combinedTraits;
        
    }else {
        NSArray *traitArray = [NSArray arrayWithObjects: any, any, nil];
        UITraitCollection *combinedTraits = [UITraitCollection traitCollectionWithTraitsFromCollections: traitArray];
        return combinedTraits;
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SecondSegue"]) {
        
              SecondVC *dist = segue.destinationViewController;
              dist.jsonData = self.jsonDatas;
    }
}

@end
