//
//  main.m
//  cameraApp
//
//  Created by naka on 2019/10/02.
//  Copyright © 2019 naka. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

