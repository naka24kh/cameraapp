//
//  ViewController+DetailVC.m
//  cameraApp
//
//  Created by naka on 2019/10/04.
//  Copyright © 2019 naka. All rights reserved.
//

#import "DetailVC.h"

#import <UIKit/UIKit.h>

@interface DetailVC ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *dIndicator;


@end


@implementation DetailVC



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _detailView.navigationDelegate = self;
    [_dIndicator startAnimating];
    
    NSURL *url = [NSURL URLWithString:_detailUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    // リクエストを投げる
    [_detailView loadRequest:request];
    
    _cameraTag.text = _tagName;
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [_dIndicator stopAnimating];
}


@end
