//
//  ViewController.m
//  cameraApp
//
//  Created by naka on 2019/10/02.
//  Copyright © 2019 naka. All rights reserved.
//


#import "FirstVC.h"
#import "BaseVC.h"
#import "Reachability.h"

@interface FirstVC ()

@property (weak, nonatomic) IBOutlet UILabel *errorText;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (nonatomic) NSUserDefaults *userDF;

@end


@implementation FirstVC

 


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _errorText.hidden = true;
    _passwardTF.secureTextEntry = true;
    _loginButton.layer.cornerRadius = 5;
    
    
    _userTF.delegate = self;
    _passwardTF.delegate = self;
    
    //ユーザIDとパスワードの呼び出し
    _userDF = [NSUserDefaults standardUserDefaults];
    _userTF.text = [_userDF stringForKey:@"idText"];
    _passwardTF.text = [_userDF stringForKey:@"passText"];

}



- (IBAction)loginEnter:(id)sender {
    
    NSString *idText = _userTF.text;
    NSString *passText = _passwardTF.text;
    NSString *userUUID = [[UIDevice currentDevice].identifierForVendor UUIDString];
   
    NSLog(@"\n idText:%@ \n passText:%@ \n userUUID:%@",idText,passText,userUUID);
 
     Reachability *reachability = [Reachability reachabilityForInternetConnection];
       NetworkStatus netStatus = [reachability currentReachabilityStatus];

       switch (netStatus) {
           case NotReachable:  //圏外
               NSLog(@"圏外");
               break;
           case ReachableViaWWAN:  //3G
               NSLog(@"3G");
               break;
           case ReachableViaWiFi:  //WiFi
               NSLog(@"WiFi");
               break;
           default:  //上記以外
               NSLog(@"上記以外");
               break;
       }

       // 用途としては下記のようにif文でネットワークステータスを判定して使うことが多そう
       if (netStatus == NotReachable) {
           _errorText.text = @"インターネットに接続できません。\n通信状態を確認してください。";
           _errorText.hidden = false;
           
       } else {
           NSLog(@"正常");
           // ここに正常系の処理を書く
             [self jsonOpen:idText userPass:passText userUUID:userUUID];
       }
  
    
  
}

-(IBAction)closeKeyBoard:(UITextField *)sender{
}

//打ち込まれたデータからJSONを開く
-(void)jsonOpen: (NSString *)userID  userPass: (NSString *)userPass userUUID: (NSString *)userUUID {
    
NSString *urlString = [NSString stringWithFormat:@"http://153.122.31.74/shipcamera/api/getip.html?user=%@&pw=%@&terminal=%@",userID, userPass, userUUID];
    
    NSLog(@"%@",urlString);
    
NSURL *url = [NSURL URLWithString:urlString];
NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil  ];

    
    
NSURLSessionDataTask *jsonData = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    //クライアントサイドのエラー（ホストに接続できないなど）か？
    if (!error) {
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *) response;
        //サーバーサイドエラーか？
        if (httpResp.statusCode == 200) {
            NSError *jsonError;

            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
            if (!jsonError) {
                // something for json data
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    [self LoadDatas:jsonArray];
      
                });
                
                
            }
        } else {
            NSLog(@"\nサーバサイドエラー　ステータスコード：%ld",(long)httpResp.statusCode);
        }
    } else {
        NSLog(@"\nクライアントサイドエラー：%@",error.localizedDescription);
    }
    
}];
    
[jsonData resume];
}



//ユーザIDのチェック
-(void)LoadDatas:(NSArray*)datas
{
    if(datas){
        NSString *status = [datas valueForKeyPath:@"response.status"];
        
        if([status  isEqual: @"OK"]){
            _firstArray = datas;
            
            //ユーザIDとパスワードの保存
            NSString *idText = _userTF.text;
            NSString *passText = _passwardTF.text;
             [_userDF setObject:idText forKey:@"idText"];
             [_userDF setObject:passText forKey:@"passText"];
             [_userDF synchronize];
             
           [self performSegueWithIdentifier:@"loginSegue" sender:self];
            
            
        }else if([status isEqual:@"NG"]){
            _errorText.text = [datas valueForKeyPath:@"response.message"];
            _errorText.hidden = false;
        }
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"loginSegue"]) {
        
              BaseVC *dist = segue.destinationViewController;
              dist.jsonDatas = self.firstArray;
    }
}



@end
