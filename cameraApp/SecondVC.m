//
//  ViewController+SecondVC.m
//  cameraApp
//
//  Created by naka on 2019/10/04.
//  Copyright © 2019 naka. All rights reserved.
//

#import "SecondVC.h"
#import "DetailVC.h"
#import "BaseVC.h"

#import <UIKit/UIKit.h>

@interface SecondVC ()
@property (nonatomic)  NSArray *ip_list;
@property (nonatomic) NSNumber *itemCount;
@property (nonatomic) NSArray *names;
@property (nonatomic) NSArray *ips;
@property (nonatomic) NSArray *ports;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator_a;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator_b;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator_c;

@end


@implementation SecondVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSLog(@"JSONData:\n %@",_jsonData);

    _ip_list = [_jsonData valueForKeyPath:@"ip_list"];
    _itemCount = [NSNumber numberWithShort:[_ip_list count]];
    NSLog(@"%@",_itemCount);
    
    _names = [_ip_list valueForKeyPath:@"name"];
    _ips = [_ip_list valueForKeyPath:@"ip"];
    _ports = [_ip_list valueForKeyPath:@"port"];
    
    _userLabel.text = [NSString stringWithFormat:@"ユーザ名:%@",[_jsonData valueForKeyPath:@"user_info.user_name"]];
    
        
    // デリゲート
    _web_a.navigationDelegate = self;
    _web_b.navigationDelegate = self;
    _web_c.navigationDelegate = self;
    
    
    [_indicator_a startAnimating];
    [_indicator_b startAnimating];
    [_indicator_c startAnimating];
    
    
    [self setView:_itemCount buttonNames:_names cameraIPs:_ips cameraPorts:_ports];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
}

//カメラの数を確認、それぞれにデータを入れる。
-(void)setView:(NSNumber*)item buttonNames:(NSArray *)buttonNames cameraIPs:(NSArray *)cameraIPs cameraPorts:(NSArray *)cameraPorts {
    
if (item == [NSNumber numberWithShort:1]) {
    _secondStack.hidden = true;
    _thirdStack.hidden = true;
    [_button_a setTitle:buttonNames[0] forState: normal];
    [self openURL_a:cameraIPs[0] cameraPort:cameraPorts[0]];
    
}else if(item == [NSNumber numberWithShort:2]) {
    _thirdStack.hidden = true;
    [_button_a setTitle:buttonNames[0] forState: normal];
    [_button_b setTitle:buttonNames[1] forState: normal];
    [self openURL_a:cameraIPs[0] cameraPort:cameraPorts[0]];
    [self openURL_b:cameraIPs[1] cameraPort:cameraPorts[1]];
    
}else{
    [_button_a setTitle:buttonNames[0] forState: normal];
    [_button_b setTitle:buttonNames[1] forState: normal];
    [_button_c setTitle:buttonNames[2] forState: normal];
    [self openURL_a:cameraIPs[0] cameraPort:cameraPorts[0]];
    [self openURL_b:cameraIPs[1] cameraPort:cameraPorts[1]];
    [self openURL_c:cameraIPs[2] cameraPort:cameraPorts[2]];
    
}

}



- (IBAction)button_aAc:(id)sender {
    [self performSegueWithIdentifier:@"detailSegue_a" sender:self];
}
- (IBAction)button_bAc:(id)sender {
    [self performSegueWithIdentifier:@"detailSegue_b" sender:self];
}
- (IBAction)button_cAc:(id)sender {
    [self performSegueWithIdentifier:@"detailSegue_c" sender:self];
}


-(void)openURL_a:(NSString *)cameraIP cameraPort:(NSString *)cameraPort {
    
    NSString *URL =[NSString  stringWithFormat: @"http://%@:%@/",cameraIP, cameraPort];
    NSURL *url = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSLog(@"open_a:%@",URL);
    // リクエストを投げる
    [_web_a loadRequest:request];
}
-(void)openURL_b:(NSString *)cameraIP cameraPort:(NSString *)cameraPort {
    
    NSString *URL =[NSString  stringWithFormat: @"http://%@:%@/",cameraIP, cameraPort];
    NSURL *url = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSLog(@"open_b:%@",URL);
    // リクエストを投げる
    [_web_b loadRequest:request];
}
-(void)openURL_c:(NSString *)cameraIP cameraPort:(NSString *)cameraPort {
    
    NSString *URL =[NSString  stringWithFormat: @"http://%@:%@/",cameraIP, cameraPort];
    NSURL *url = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSLog(@"open_c:%@",URL);
    // リクエストを投げる
    [_web_c loadRequest:request];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [_indicator_a stopAnimating];
    [_indicator_b stopAnimating];
    [_indicator_c stopAnimating];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if ([segue.identifier isEqualToString:@"detailSegue_a"]) {
        
        DetailVC *dist = segue.destinationViewController;

        dist.tagName = self.button_a.titleLabel.text;
        dist.detailUrl = [NSString  stringWithFormat: @"http://%@:%@/",_ips[0], _ports[0]];
        
        }else if([segue.identifier isEqualToString:@"detailSegue_b"]) {
             DetailVC *dist = segue.destinationViewController;
                   
            dist.tagName = self.button_b.titleLabel.text;
            dist.detailUrl = [NSString  stringWithFormat: @"http://%@:%@/",_ips[1], _ports[1]];
            
        }else if([segue.identifier isEqualToString:@"detailSegue_c"]) {
             DetailVC *dist = segue.destinationViewController;
                   
            dist.tagName = self.button_c.titleLabel.text;
            dist.detailUrl = [NSString  stringWithFormat: @"http://%@:%@/",_ips[2], _ports[2]];
    }
}

-(IBAction)myUnwindAction:(UIStoryboardSegue *)unwindSegue{
}



@end
