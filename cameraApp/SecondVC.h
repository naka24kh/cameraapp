//
//  ViewController+SecondVC.h
//  cameraApp
//
//  Created by naka on 2019/10/04.
//  Copyright © 2019 naka. All rights reserved.
//

#import <UIKit/UIKit.h>

#import<WebKit/WebKit.h>


@interface SecondVC : UIViewController <WKNavigationDelegate>

@property (weak, nonatomic) IBOutlet UILabel *userLabel;

@property (weak, nonatomic) IBOutlet UIStackView *FirstStack;
@property (weak, nonatomic) IBOutlet UIButton *button_a;
@property (weak, nonatomic) IBOutlet WKWebView *web_a;

@property (weak, nonatomic) IBOutlet UIStackView *secondStack;
@property (weak, nonatomic) IBOutlet UIButton *button_b;
@property (weak, nonatomic) IBOutlet WKWebView *web_b;

@property (weak, nonatomic) IBOutlet UIStackView *thirdStack;
@property (weak, nonatomic) IBOutlet UIButton *button_c;
@property (weak, nonatomic) IBOutlet WKWebView *web_c;

@property (weak, nonatomic) NSArray *jsonData;

@end
